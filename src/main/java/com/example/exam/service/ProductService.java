package com.example.exam.service;

import com.example.exam.domain.Product;
import com.example.exam.exception.ResourceNotFoundException;
import com.example.exam.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductService {
    private final Logger log = LoggerFactory.getLogger(ProductService.class);

    @Autowired
    private ProductRepository productRepository;

    @Transactional(readOnly = true)
    public List<Product> getAllProducts(){
        log.debug("Request to get all Bands");
        return productRepository.findAll();
    }

    public Product saveOrUpdateProduct(Product product){
        return productRepository.save(product);
    }

    public void sellProduct(Long id, int quantity){
        productRepository.findById(id).map(product -> {
            product.setQuantity(quantity);
            return productRepository.save(product);
        });
    }

    @Transactional(readOnly = true)
    public Product findById(Long id) {
        log.debug("Request to get Band : {}",id);
        return productRepository.getById(id);
    }

    public void delete(Long id){
        Product product = findById(id);
        productRepository.delete(product);
    }
}
