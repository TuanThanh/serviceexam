package com.example.exam.webapp;

import com.example.exam.domain.Product;
import com.example.exam.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/product")
@Transactional
public class ProductResource {
    private final Logger log = LoggerFactory.getLogger(ProductResource.class);

    @Autowired
    private ProductService productService;

    @GetMapping("/")
    public List<Product> getAllProduct(){
        log.debug("REST request to get all Product");
        return productService.getAllProducts();
    }

    @GetMapping("/{id}")
    public Product getAccount(@PathVariable long id){
        log.debug("REST request to get account : {}", id);
        return productService.findById(id);
    }

    @PostMapping("/")
    public ResponseEntity<?> addProduct(@Valid @RequestBody Product product, BindingResult result){
        if(result.hasErrors()){
            Map<String, String> errorMap = new HashMap<>();

            for(FieldError error: result.getFieldErrors()){
                errorMap.put(error.getField(), error.getDefaultMessage());
            }

            return new ResponseEntity<Map<String, String>>(errorMap, HttpStatus.BAD_REQUEST);
        }
        Product newProduct = productService.saveOrUpdateProduct(product);
        return new ResponseEntity<Product>(newProduct, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public void updateProduct(@PathVariable Long id, @Valid @RequestBody Product product){
        productService.sellProduct(id, product.getQuantity());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable Long id){
        productService.delete(id);
        return new ResponseEntity<String>("Product deleted", HttpStatus.OK);
    }


}
